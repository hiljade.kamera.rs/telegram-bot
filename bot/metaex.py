import PIL.Image
import utils as ut
from PIL.ExifTags import TAGS, GPSTAGS

OSM_LINK = 'https://www.openstreetmap.org/?mlat=LAT&mlon=LON'

def get_data(path):
    try:
        img = PIL.Image.open(path)
    except:
        raise Exception('Fajl {} se ne moze otvoriti ili ne postoji'.format(path))
    lbld = {}
    for (k,v) in img._getexif().items():
        lbld[TAGS.get(k)] = v
    return lbld

def get_coords(exif_data): 
    ret = {}
    if 'GPSInfo' not in exif_data:
        return None
    gps = exif_data['GPSInfo']
    lat_dir = gps[1]
    lat_data = gps[2]
    lon_dir = gps[3]
    lon_data = gps[4]
    
    if ut.is_number(lon_data[0][0]) and ut.is_number(lon_data[1][0]) and ut.is_number(lon_data[2][0]) \
       and ut.is_number(lon_data[2][1]) and ut.is_number(lat_data[0][0]) and ut.is_number(lat_data[1][0])\
       and ut.is_number(lat_data[2][0]) and ut.is_number(lat_data[2][1]):
        ret['lon'] = (lon_dir, lon_data[0][0], lon_data[1][0], lon_data[2][0]/float(lon_data[2][1]))
        ret['lat'] = (lat_dir, lat_data[0][0], lat_data[1][0], lat_data[2][0]/float(lon_data[2][1]))
        return ret
    else:
        return None

def print_coords(coords):
    print('{} {} {} {}'.format(coords['lon'][1], coords['lon'][2], coords['lon'][3], coords['lon'][0]))
    print('{} {} {} {}'.format(coords['lat'][1], coords['lat'][2], coords['lat'][3], coords['lat'][0]))

def deg_to_float(d,m,s):
    return d + (m / 60.0) + (s / 3600.0)

def main():
    path = input()
    data = get_data(path)
    coords = get_coords(data)
    lat = deg_to_float(coords['lat'][1], coords['lat'][2], coords['lat'][3])
    lon = deg_to_float(coords['lon'][1], coords['lon'][2], coords['lon'][3])
    link = OSM_LINK.replace('LAT', str(lat)).replace('LON', str(lon))
    print(link)
    #print_coords(coords)
if __name__ == '__main__':
    main()
