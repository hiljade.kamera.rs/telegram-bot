import utils as ut


class Camera():
    def __init__(self, lat=None, lon=None, camera_type='goverment', area='public'):
        self.lat = lat
        self.lon = lon
        self.area = area
        self.camera_type = camera_type

    def set_area(self, area):
        self.area = area

    def set_camera_type(self, camera_type):
        self.cameraType = camera_type

    def set_coords(self, lat, lon):
        if ut.is_number(lat) and ut.is_number(lon):
            self.lat = lat
            self.lon = lon
        else:
            raise Exception('Bad argument types for  set_coords method, expected floats got lat={} and lon={}'
                            .format(lat, lon))

    def to_json(self):
        return {
            'lat': self.lat,
            'lon': self.lon,
            'type': self.camera_type
        }

    def __repr__(self):
        return str(self.__dict__)
