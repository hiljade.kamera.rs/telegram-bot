def is_float(val):
    '''Checks if value is of floating type'''
    return isinstance(val, float)

def is_integer(val):
    '''Checks if value is of integer type'''
    return isinstance(val, int)

def is_number(val):
    '''Checks if value is of number type, that is float or integer'''
    return is_float(val) or is_integer(val)
