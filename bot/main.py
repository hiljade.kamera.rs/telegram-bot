from telegram.ext import Updater, InlineQueryHandler, CommandHandler, MessageHandler, Filters
from telegram import Update
from Camera import Camera
import requests
import os
import metaex
import json
import logging

cameras = {}


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)


def website(bot, update):
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text="https://hiljade.kamera.rs/")


def help(bot, update):
    chat_id = update.message.chat_id
    bot.send_message(
        chat_id=chat_id, text="List of commands: /website or you can upload image as file or add location")



def location_input(bot, update):
    chat_id = update.message.chat_id
    location = update.message.location
    print(location)
    set_location(chat_id, location['longitude'], location['latitude'])
    bot.send_message(chat_id=chat_id, text=str(location))
    print(cameras)

def area(bot, update, args):
    print('Area!')
    chat_id = update.message.chat_id
    print(args)
    global cameras
    if chat_id not in cameras:
        cameras[chat_id] = Camera(area=args[0])
    else:
        cameras[chat_id].set_area(args[0])
    print(cameras)
    bot.send_message(chat_id=chat_id, text="Ok!")


def camera_type(bot, update, args):
    print('Type!')
    chat_id = update.message.chat_id
    print(args)
    global cameras
    if chat_id not in cameras:
        cameras[chat_id] = Camera(camera_type=args[0])
    else:
        cameras[chat_id].set_camera_type(args[0])
    print(cameras)
    bot.send_message(chat_id=chat_id, text="Ok!")


def send(bot, update):
    print('Sending')
    chat_id = update.message.chat_id
    global cameras
    if chat_id not in cameras:
        print('no data')
        bot.send_message(chat_id=chat_id, text='No data to send.')
    elif cameras[chat_id].lat is None:
        print('no location')
        bot.send_message(chat_id=chat_id, text='Location is required.')
        print(cameras[chat_id])
    else:
        print('all good')
        bot.send_message(chat_id=chat_id, text='Sending camera!')
        print(cameras[chat_id])
        body = cameras[chat_id].to_json()
        print(body)
        headers = {'Content-type': 'application/json'}
        r = requests.post('https://hiljade.kamera.rs/v1/cameras', data=json.dumps(body), headers=headers)
        print(cameras[chat_id])
        print(r.text)
        del cameras[chat_id]


def set_location(chat_id, lat, lon):
    global cameras
    if chat_id not in cameras:
        cameras[chat_id] = Camera(lat, lon)
    else:
        cameras[chat_id].set_coords(lat, lon)


def location_from_image(bot, update):
    file_id = update.message.document.file_id
    new_file = bot.getFile(file_id)
    file_name = update.message.document.file_name
    new_file.download(file_name)
    chat_id = update.message.chat_id
    data = {}
    try:
        data = metaex.get_data(file_name)
    except:
        bot.send_message(
            chat_id=chat_id, text="This image has no location in metadata")
        return
    coords = metaex.get_coords(data)
    if(coords is not None):
        lat = metaex.deg_to_float(
            coords['lat'][1], coords['lat'][2], coords['lat'][3])
        lon = metaex.deg_to_float(
            coords['lon'][1], coords['lon'][2], coords['lon'][3])
        print(lat)
        print(lon)
        bot.send_message(chat_id=chat_id, text="Got location from image!")
    else:
        print("No location")
        bot.send_message(
            chat_id=chat_id, text="This image has no location in metadata")
    os.remove(file_name)


def main():
    updater = Updater('1135372293:AAGyERbzoqaRud0LrWfKbVyG-qtC4mAfJuQ')
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('website', website))
    dp.add_handler(CommandHandler('help', help))
    dp.add_handler(CommandHandler('area', area, pass_args=True))
    dp.add_handler(CommandHandler('type', camera_type, pass_args=True))
    dp.add_handler(CommandHandler('send', send))
    dp.add_handler(MessageHandler(Filters.location, location_input))
    dp.add_handler(MessageHandler(Filters.document, location_from_image))
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
